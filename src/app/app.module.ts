import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms'


import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { RepeatSectionComponent } from './components/repeat-section/repeat-section.component';
import { InputfileComponent } from './components/inputfile/inputfile.component';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatStepperModule } from '@angular/material/stepper';
import { SuccessComponent } from './components/success/success.component';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { PhonePickerComponent } from './components/phone-picker/phone-picker.component';
import { MatIconModule } from '@angular/material/icon';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';




export function playerFactory() {
  return player;
}

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

const maskConfig: Partial<IConfig> = {
  validation: true,
};

@NgModule({
  declarations: [
    AppComponent,
    ReactiveFormComponent,
    RepeatSectionComponent,
    InputfileComponent,
    SuccessComponent,
    PhonePickerComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    NoopAnimationsModule,
    FormlyMaterialModule,
    LottieModule.forRoot({ player: playerFactory }),
    FormlyModule.forRoot({
      types: [
        // Manera de hacer los customs inputs
        { name: 'repeat', component: RepeatSectionComponent },
        { name: 'file', component: InputfileComponent, wrappers: ['form-field'] },
        { name: 'phone', component: PhonePickerComponent },
      ],
      validationMessages: [{ name: 'required', message: 'Este campo es requerido' }]
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxMaskModule.forRoot(maskConfig),
    NgxIntlTelInputModule,
    MatIconModule,
    FormlyModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
