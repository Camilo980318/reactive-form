import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { SuccessComponent } from './components/success/success.component';


const routes: Routes = [

  { path: '', component: ReactiveFormComponent },
  { path: 'success', component: SuccessComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
