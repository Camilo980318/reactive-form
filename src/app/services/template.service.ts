import { Injectable } from '@angular/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

// Definimos los atributos que tendrá el json
export interface StepType {
  label: string;
  info: string;
  text: string;
  fields: FormlyFieldConfig[];
}

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  // Json del formly
  steps: StepType[] = [
    {
      label: 'Datos personales',
      info: 'Datos',
      text: '',
      fields: [
        {
          fieldGroupClassName: 'row',
          fieldGroup: [{
            className: 'col-12',
            key: 'name',
            type: 'input',
            templateOptions: {
              label: 'Nombres',
              placeholder: 'Ingresa tu Nombre',
              required: true,
            },
          },
          {
            className: 'col-12',
            key: 'lastName',
            type: 'input',
            templateOptions: {
              label: 'Apellidos',
              placeholder: 'Ingresa tus Apellidos',
              required: true,
            }
          }],
        },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [{
            className: 'col-12',
            key: 'phone',
            type: 'phone',
            templateOptions: {
              label: 'Número de teléfono',
              placeholder: '(___) ___-____',
              required: true,
            }
          },
          {
            className: 'col-12',
            key: 'email',
            type: 'input',
            templateOptions: {
              label: 'Correo Electrónico',
              placeholder: 'Ingresa tu Email',
              required: true,
            }
          }]
        }
      ],
    },
    {
      label: 'Términos y condiciones',
      info: 'Términos',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      fields: [
        {
          fieldGroupClassName: 'row',
          fieldGroup: [{
            className: 'col-12',
            key: 'conditions',
            type: 'checkbox',
            templateOptions: {
              label: 'Acepto Términos y condiciones.',
              required: true,
            },
          }]
        }]
    },
    {
      label: 'Información detallada',
      info: 'Detalles',
      text: '',
      fields: [

        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-12',
              key: 'rooms',
              type: 'input',
              templateOptions: {
                label: 'Número de habitaciones',
                placeholder: 'Ingresa el Número',
                type: 'number',
                required: true
              }

            },
            {
              className: 'col-12',
              key: 'garage',
              type: 'select',
              templateOptions: {
                label: '¿Tiene Garage?',
                options: [
                  { id: '1', name: 'Si' },
                  { id: '0', name: 'No' },
                ],
                valueProp: 'id',
                labelProp: 'name',
                required: true
              }

            },
            {
              className: 'col-12',
              key: 'numGarage',
              type: 'input',
              templateOptions: {
                label: '¿Para cuántos coches?',
                placeholder: 'Ingresa el Número',
                type: 'number',
                required: true
              },
              hideExpression: "!model.garage || model.garage == '0'"

            },
          ]
        },
        {
          fieldGroupClassName: 'row',
          fieldGroup: [
            {
              className: 'col-12',
              key: 'bathrooms',
              type: 'input',
              templateOptions: {
                label: '¿Cuántos baños tienes?',
                placeholder: 'Ingresa el Número',
                type: 'number',
                required: true
              }
            },
            {
              className: 'col-12',
              key: 'allShowers',
              type: 'select',
              templateOptions: {
                label: '¿Todos tienen ducha?',
                options: [
                  { id: '1', name: 'Si' },
                  { id: '0', name: 'No' },
                ],
                valueProp: 'id',
                labelProp: 'name',
                required: true
              }

            },
            {
              className: 'col-12',
              key: 'numShower',
              type: 'input',
              templateOptions: {
                label: 'Número de duchas',
                placeholder: 'Ingresa el Número',
                type: 'number',
                required: true
              },
              hideExpression: "!model.allShowers || model.allShowers == '1'"

            },
          ]
        },
      ],
    },
    {
      label: 'Información adicional',
      info: 'Adicional',
      text: '',
      fields: [
        {
          fieldGroupClassName: 'row',
          key: 'services',
          type: 'repeat',
          fieldArray: {
            fieldGroup: [{
              className: '',
              key: 'nameService',
              type: 'input',
              templateOptions: {
                label: 'Nombre del Servicio',
                required: false
              },

            }, {
              className: '',
              key: 'infoService',
              type: 'input',
              templateOptions: {
                label: 'Información del Servicio',
                required: false
              },
            },]
          }
        },

      ],
    },

    {
      label: 'Sube tus archivos',
      info: 'Finalizar',
      text: '',
      fields: [

        {
          fieldGroupClassName: 'row',
          fieldGroup: [{
            className: 'col-12',
            key: 'file',
            type: 'file',
          }]
        },

      ],
    },
  ]

  constructor() { }
}
