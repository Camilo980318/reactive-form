import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { TemplateService } from '../../services/template.service';
import { Router } from '@angular/router';

// Definimos la manera como van a llegar los datos del service
export interface StepType {
  label: string;
  info: string;
  text: string;
  fields: FormlyFieldConfig[];
}

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss'],

})
export class ReactiveFormComponent implements OnInit {

  // Configuraciones del formly
  steps: StepType[] = [];
  activedStep = 0;
  model: any = {};

  form = new FormArray(this.steps.map(() => new FormGroup({})));
  options = this.steps.map(() => <FormlyFormOptions>{});


  constructor(private templateService: TemplateService, private router: Router) { }

  ngOnInit(): void {
    this.steps = this.templateService.steps;
  }

  prevStep(step) {
    this.activedStep = step - 1;
  }

  nextStep(step) {
    this.activedStep = step + 1;
  }

  submit() {
    console.log(this.model)
    this.router.navigate(['/success'])
  }

  next() {
    console.log("Something")
  }
}


