import { Component, OnInit } from '@angular/core';
import { FieldArrayType } from '@ngx-formly/core';

@Component({
  selector: 'app-repeat-section',
  templateUrl: './repeat-section.component.html',
  styleUrls: ['./repeat-section.component.css']
})

// Extiende del FieldArray del formly para que se guarden los datos en un array
export class RepeatSectionComponent extends FieldArrayType { }
