import { Component, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'app-inputfile',
  templateUrl: './inputfile.component.html',
  styleUrls: ['./inputfile.component.css']
})
export class InputfileComponent extends FieldType {

  fileName: string;

  getNameFile(img: File) {
    this.fileName = img.name;
  }
}
