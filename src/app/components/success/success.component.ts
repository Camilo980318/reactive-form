import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';
import { AnimationItem } from 'lottie-web';


@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {

  constructor() { }

  // Obtenemos la animación
  options: AnimationOptions = {
    path: './assets/animations/success.json',
    loop: false,
  };

  // Configuramos la animación
  animationCreated(animation: AnimationItem) {
    animation.play();
    animation.setSpeed(0.9);
  }

  ngOnInit(): void {
  }

}
